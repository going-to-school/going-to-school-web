import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SigninComponent } from './user/signin/signin.component';
import { SignupComponent } from './user/signup/signup.component';
import { WardComponent } from './ward/ward.component';
import { AuthGuard } from './user/auth-guard.service';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {CreateUserComponent} from "./user/create-user/create-user.component";

const routes: Routes = [
  { path: '', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  {path: 'administrator-dashboard', canActivate: [AuthGuard], component: DashboardComponent},
  { path: 'ward', canActivate: [AuthGuard], component: WardComponent },
  { path: "user", canActivate: [AuthGuard], component: CreateUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
