import { Component} from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './user/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css','./ag-grid.css','./ag-theme-balham.css']
})

export class AppComponent {
  isAuthenticated = false;

  constructor(private authService: AuthService,
              private router: Router) {
    this.authService.authStatusChanged.subscribe(
        (authenticated) => {
          this.isAuthenticated = authenticated;
          if (authenticated) {
            this.router.navigate(['/user']);
          } else {
            this.router.navigate(['/']);
          }
        }
    );

    this.authService.initAuth();


  }

  onLogout() {
    this.authService.logout();
  }
}
