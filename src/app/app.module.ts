import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {AgGridModule} from '@ag-grid-community/angular';

import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './user/auth.service';
import { AppComponent } from './app.component';

import { SignupComponent } from './user/signup/signup.component';
import { SigninComponent } from './user/signin/signin.component';
import {WardComponent} from "./ward/ward.component";
import {WardInputComponent} from "./ward/ward-input/ward-input.component";
import {WardResultsComponent} from "./ward/ward-results/ward-results.component";
import {WardService} from "./ward/ward.service";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {AdministratorDashboardComponent} from "./dashboard/administrator-dashboard/administrator-dashboard.component";
import {CreateUserComponent} from "./user/create-user/create-user.component";
import {UserService} from "./user/user.service";

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    WardComponent,
    WardInputComponent,
    WardResultsComponent,
    DashboardComponent ,
    AdministratorDashboardComponent,
    CreateUserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    AppRoutingModule
  ],
  providers: [AuthService, WardService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
