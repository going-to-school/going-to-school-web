import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-administrator-dashboard',
    templateUrl: './administrator-dashboard.component.html'
})

export class AdministratorDashboardComponent {
    @ViewChild('administratorDashboardForm') form: NgForm;
    isLoading = false;
    couldNotLoadData = false;

    constructor(private router: Router) {
    }

    onCreateUser() {
        this.router.navigate(['/create-user']);
    }
}
