import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

import {
  CognitoUserPool,
  CognitoUserAttribute,
  CognitoUser,
  AuthenticationDetails,
  CognitoUserSession
} from 'amazon-cognito-identity-js';

import { User } from './user.model';

const POOL_DATA = {
  UserPoolId: 'ap-south-1_NqXxkW8es',
  ClientId: '30nid5rfimclvm6hqklavut9dp'
};
const userPool = new CognitoUserPool(POOL_DATA);

@Injectable()
export class AuthService {
  authIsLoading = new BehaviorSubject<boolean>(false);
  authDidFail = new BehaviorSubject<boolean>(false);
  authStatusChanged = new Subject<boolean>();
  registeredUser: CognitoUser;
  httpHeaders = new HttpHeaders();
  user: User;

  constructor(private router: Router,
              private httpClient: HttpClient) {
  }

  signUp(user: User): string {
    this.authIsLoading.next(true);

    const attrList: CognitoUserAttribute[] = [];
    const emailAttribute = {
      Name: 'email',
      Value: user.email
    };

    attrList.push(new CognitoUserAttribute(emailAttribute));
    userPool.signUp(user.username, user.password, attrList, null, (err, result) => {
      if (err) {
        this.authDidFail.next(true);
        this.authIsLoading.next(false);
        return 'failure';
      }
      this.authDidFail.next(false);
      this.authIsLoading.next(false);
      this.registeredUser = result.user;
    });
    return 'success';
  }

  confirmUser(username: string, code: string) {
    this.authIsLoading.next(true);
    const userData = {
      Username: username,
      Pool: userPool
    };
    const cognitUser = new CognitoUser(userData);
    cognitUser.confirmRegistration(code, true, (err, result) => {
      if (err) {
        this.authDidFail.next(true);
        this.authIsLoading.next(false);
        return;
      }
      this.authDidFail.next(false);
      this.authIsLoading.next(false);
      this.router.navigate(['/']);
    });
  }

  signIn(username: string, password: string): string {
    this.authIsLoading.next(true);
    const authData = {
      Username: username,
      Password: password
    };
    const authDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: username,
      Pool: userPool
    };
    const cognitoUser = new CognitoUser(userData);
    const that = this;
    cognitoUser.authenticateUser(authDetails, {
      onSuccess (result: CognitoUserSession) {
        that.authStatusChanged.next(true);
        that.authDidFail.next(false);
        that.authIsLoading.next(false);
        that.fetchUserData(username);
      },
      onFailure(err) {
        that.authDidFail.next(true);
        that.authIsLoading.next(false);
      }
    });
    this.authStatusChanged.next(true); // create user with cognito data
    return status;
  }

  fetchUserData(username: String) {
    let queryParam = '?username=' + username;

    this.getAuthenticatedUser().getSession((err, session) => {
      if (err) {
        return;
      }
      this.httpClient.get('https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/user/login' + queryParam, {
        headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
      })
          .subscribe(
              (data : User[]) => {
                const users = JSON.stringify(data);
                const parsedData = JSON.parse(users);
                this.user = parsedData.data[0];
              },
              (error) => {
                console.log(error);
                this.logout();
              }
          );
    });

  }

  onChangePassword(email: string, verificationCode: string, password: string) {
    const userData = {
      Username: email,
      Pool: userPool
    };

    const cognitoUser = new CognitoUser(userData);
    cognitoUser.confirmPassword(verificationCode, password, {
      onSuccess() {
        console.log("password successfully changed");
      },
      onFailure(err: Error) {
        console.log(err);
      }
    });
  }

  onRetrievePassword(email: string) {
    const userData = {
      Username: email,
      Pool: userPool
    };

    const cognitoUser = new CognitoUser(userData);
    cognitoUser.forgotPassword({
      onSuccess (data: any) {
        console.log(data);
      },
      onFailure(err) {
        console.log(err);
      }
    });
  }

  getAuthenticatedUser() {
    return userPool.getCurrentUser();
  }

  logout() {
    this.getAuthenticatedUser().signOut();
    this.authStatusChanged.next(false);
  }

  isAuthenticated(): Observable<boolean> {
    const user = this.getAuthenticatedUser();
    const obs = Observable.create((observer) => {
      if (!user) {
        observer.next(false);
      } else {
        user.getSession((err, session) => {
          if (err) {
            observer.next(false);
          } else {
            if (session.isValid()) {
              observer.next(true);
            } else {
              observer.next(false);
            }
          }
        });
      }
      observer.complete();
    });
    return obs;
  }

  initAuth() {
    this.isAuthenticated().subscribe(
      (auth) => this.authStatusChanged.next(auth)
    );
  }
}
