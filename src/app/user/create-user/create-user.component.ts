import { Component, ViewChild } from '@angular/core';
import { User } from '../user.model';
import { NgForm } from '@angular/forms';

import { AuthService } from '../auth.service';
import {UserService} from "../user.service";

@Component({
    selector: 'app-create-user',
    templateUrl: './create-user.component.html'
})

export class CreateUserComponent {
    @ViewChild('createUserForm') form: NgForm;

    public schools = [
        {id: 1, name: 'school1'},
        {id: 1, name: 'school2'}
    ];
    constructor(private authService: AuthService,
                private userService: UserService) {
        // this.schools = [
        //     {id: 1, name: 'school1', ward: 'ward1'}
        // ];
    }

    randomString(chars) {
        let mask = '';
        if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
        if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if (chars.indexOf('#') > -1) mask += '0123456789';
        if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
        let result = '';
        for (let i = 20; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
        return result;
    }

    createUser(user: User) {
        this.userService.onStoreData(user);
    }

    onSubmit() {
        const user: User = {
            name: this.form.value.name,
            username: this.form.value.username,
            password: this.randomString("#a!A"),
            email: this.form.value.email,
            address: this.form.value.address,
            phone: this.form.value.phone,
            school: 1,
            role: 1
        };

        const data  = this.authService.signUp(user);
        if(data === 'success') {
            this.createUser(user);
        }
    }
}
