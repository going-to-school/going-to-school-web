import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth.service';
import {UserService} from "../user.service";
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html'
})
export class SigninComponent implements OnInit {
  @ViewChild('usrForm') form: NgForm;
  @ViewChild('forgotPwdForm') forgotPwdForm: NgForm;
  @ViewChild('confirmPwdForm') changePwdForm: NgForm;
  @ViewChild('confirmForm') confirmForm: NgForm;
  didFail = false;
  isLoading = false;
  formName = "signIn";

  constructor(private authService: AuthService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.authService.authIsLoading.subscribe(
      (isLoading: boolean) => this.isLoading = isLoading
    );
    this.authService.authDidFail.subscribe(
      (didFail: boolean) => this.didFail = didFail
    );
  }

  onSubmit() {
    this.authService.signIn(this.form.value.username, this.form.value.password);
  }

  forgotPasswordForm() {
    this.formName = "forgotPassword";
  }

  onRetrievePassword() {
    this.authService.onRetrievePassword(this.forgotPwdForm.value.email);
    this.formName = "changePassword";
  }

  onChangePassword() {
    this.authService.onChangePassword(this.changePwdForm.value.confirmEmail,
        this.changePwdForm.value.confirmationCode,
        this.changePwdForm.value.newPassword);
    this.formName = "signIn";
  }

  confirmUserForm() {
    this.formName = "confirmUser";
  }

  confirmUser() {
    this.authService.confirmUser(this.confirmForm.value.usrName, this.confirmForm.value.validationCode);
    this.formName = "signIn";
  }
}
