export interface User {
  name: string;
  username: string;
  email: string;
  password: string;
  address: string;
  phone: number;
  school: number;
  role: number;
}
