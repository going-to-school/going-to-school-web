import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';
// import 'rxjs/add/operator/map';

import { AuthService } from './auth.service';
import {User} from "./user.model";

@Injectable()
export class UserService {
    dataEdited = new BehaviorSubject<boolean>(false);
    dataIsLoading = new BehaviorSubject<boolean>(false);
    dataLoaded = new Subject<User[]>();
    dataLoadFailed = new Subject<boolean>();
    userData: User[];
    httpHeaders = new HttpHeaders();
    createUser = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/user/create';
    updateUser = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/user/update';
    deleteUser = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/user/delete';
    fetchUser = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/user';
    loggedInUser: User;

    constructor(private http: HttpClient,
                private authService: AuthService) {
    }

    onStoreData(user: User) {
        this.dataLoadFailed.next(false);
        this.dataIsLoading.next(true);
        this.dataEdited.next(false);
        this.authService.getAuthenticatedUser().getSession((err, session) => {
            if (err) {
                return;
            }
            this.http.post(this.createUser, user, {
                headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
            })
                .subscribe(
                    (result) => {
                        this.dataLoadFailed.next(false);
                        this.dataIsLoading.next(false);
                        this.dataEdited.next(true);
                    },
                    (error) => {
                        this.dataIsLoading.next(false);
                        this.dataLoadFailed.next(true);
                        this.dataEdited.next(false);
                    }
                );
        });
    }

    onRetrieveData(data: User) {
        let queryParam = '?';

        if(data.name !== '') {
            queryParam += 'name=' + data.name;
        }

        this.dataLoadFailed.next(false);
        this.authService.getAuthenticatedUser().getSession((err, session) => {
            this.http.get(this.fetchUser + queryParam, {
                headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
            })
                .subscribe(
                    (data : User[]) => {
                        if (!data) {
                            this.dataLoadFailed.next(true);
                            return;
                        }
                        this.userData = data;
                        this.dataLoaded.next(data);
                    },
                    (error) => {
                        console.log(error);
                        this.dataLoadFailed.next(true);
                        this.dataLoaded.next(null);
                    }
                );
        });
    }
    onDeleteData(data: User) {
        this.dataLoadFailed.next(false);
        this.authService.getAuthenticatedUser().getSession((err, session) => {
            this.http.post( this.deleteUser, data, {
                headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
            })
                .subscribe(
                    (data) => {
                        console.log(data);
                    },
                    (error) => this.dataLoadFailed.next(true)
                );
        });
    }

    onUpdateData(data: String) {
        this.dataLoadFailed.next(false);
        this.authService.getAuthenticatedUser().getSession((err, session) => {
            this.http.post( this.updateUser, data, {
                headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
            })
                .subscribe(
                    (data) => {
                        console.log(data);
                    },
                    (error) => this.dataLoadFailed.next(true)
                );
        });
    }
}
