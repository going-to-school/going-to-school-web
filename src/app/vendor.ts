import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';

// RxJS
import 'rxjs';

// ag-grid
import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-balham.css';

// or, if using Enterprise features
// import '@ag-grid-enterprise/all-modules/dist/styles/ag-grid.css';
// import '@ag-grid-enterprise/all-modules/dist/styles/ag-theme-balham.css';

import '@ag-grid-community/angular'
