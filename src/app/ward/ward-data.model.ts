export interface WardData {
  name: string;
  city: string;
  state: string;
  address: string;
  email: string;
  phone: number;
}
