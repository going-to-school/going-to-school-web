import { Component, ViewChild } from '@angular/core';
import { WardData } from '../ward-data.model';
import { NgForm } from '@angular/forms';

import { WardService } from '../ward.service';

@Component({
    selector: 'app-ward-input',
    templateUrl: './ward-input.component.html'
})

export class WardInputComponent {
    @ViewChild('wardForm') form: NgForm;

    constructor(private wardService: WardService) {

    }

    onSubmit() {
        const data: WardData = {
            name: this.form.value.name as string,
            city: this.form.value.city as string,
            state: this.form.value.state as string,
            email: this.form.value.email as string,
            address: this.form.value.address as string,
            phone: this.form.value.phone as number
        };
        this.wardService.onStoreData(data);
    }

    onSearchWard() {
        this.wardService.dataEdited.next(false);
    }
}
