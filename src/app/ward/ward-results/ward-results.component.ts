import { Component, OnInit, ViewChild} from '@angular/core';
import {GridOptions, Module} from '@ag-grid-community/all-modules';
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
import { WardService } from '../ward.service';

import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-balham.css';

import {WardData} from "../ward-data.model";
import {NgForm} from "@angular/forms";

@Component({
    selector: 'app-ward-results',
    templateUrl: './ward-results.component.html',
    styleUrls: ['./ward-results.component.css']
})

export class WardResultsComponent implements OnInit {
    @ViewChild('wardSearchForm') form: NgForm;
    public gridOptions:GridOptions;
    public rowData:any[];
    public columnDefs:any[];
    public modules: Module[] = [ClientSideRowModelModule];

    constructor(private wardService: WardService) {
        this.columnDefs = [
            {headerName: "Ward Name", field: "name"},
            {headerName: "City", field: "city"},
            {headerName: "State", field: "state"}
        ];
        this.rowData = [
            {name: "Ward1", city: "Celica", state: "State"},
            {name: "Ward2", city: "Mondeo", state: "State"},
            {name: "Ward3", city: "Boxter", state: "State"}
        ];

        this.gridOptions = <GridOptions>{
            columnDefs: this.columnDefs,
            rowData: this.wardService.wardData,
            onGridReady: () => {
                this.gridOptions.api.sizeColumnsToFit();
            }
        };
    }

    ngOnInit(): void {
        this.wardService.dataLoaded.subscribe(
            (data: WardData[]) => {
                this.gridOptions.rowData = data;
            }
        );
    }

    onSubmit() {
        const data: WardData = {
            name: this.form.value.name as string,
            city: this.form.value.city as string,
            state: this.form.value.state as string,
            email: '' as string,
            address: '' as string,
            phone: 0 as number
        };

        this.wardService.onRetrieveData(data);
    }

    onCreateWard() {
        this.wardService.dataEdited.next(true);
    }

}
