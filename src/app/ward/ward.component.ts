import { Component, OnInit } from '@angular/core';
import { WardService } from './ward.service';

@Component({
  selector: 'app-ward',
  templateUrl: './ward.component.html',
  styleUrls: ['./ward.component.css']
})
export class WardComponent implements OnInit {
  search = true;
  constructor(private wardService: WardService) {}

  ngOnInit() {
    this.wardService.dataEdited.subscribe(
      (edited: boolean) => this.search = !edited
    );
  }
}
