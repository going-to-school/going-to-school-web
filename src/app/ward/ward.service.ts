import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';
// import 'rxjs/add/operator/map';

import { WardData } from './ward-data.model';
import { AuthService } from '../user/auth.service';
import {User} from "../user/user.model";

@Injectable()
export class WardService {
  dataEdited = new BehaviorSubject<boolean>(false);
  dataIsLoading = new BehaviorSubject<boolean>(false);
  dataLoaded = new Subject<WardData[]>();
  dataLoadFailed = new Subject<boolean>();
  wardData: WardData[];
  httpHeaders = new HttpHeaders();
  createWard = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/ward/create';
  updateWard = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/ward/update';
  deleteWard = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/ward/delete';
  fetchWard = 'https://y2crg4z7g5.execute-api.ap-south-1.amazonaws.com/dev/ward';

  constructor(private http: HttpClient,
              private authService: AuthService) {
  }

  onStoreData(data: WardData) {
    this.dataLoadFailed.next(false);
    this.dataIsLoading.next(true);
    this.dataEdited.next(false);
    this.authService.getAuthenticatedUser().getSession((err, session) => {
      if (err) {
        return;
      }
      this.http.post(this.createWard, data, {
        headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
      })
        .subscribe(
          (result) => {
            this.dataLoadFailed.next(false);
            this.dataIsLoading.next(false);
            this.dataEdited.next(true);
          },
          (error) => {
            this.dataIsLoading.next(false);
            this.dataLoadFailed.next(true);
            this.dataEdited.next(false);
          }
        );
    });
  }
  onRetrieveData(data: WardData) {
    let queryParam = '?';
    let searchFieldExist = false;

    if(data.name !== '') {
        queryParam += 'name=' + data.name;
        searchFieldExist = true;
    }

    if(data.city !== '') {
        if(searchFieldExist) {
            queryParam += '&';
        }

        queryParam += 'city=' + data.city;
    }
    if(data.state !== '') {
      if(searchFieldExist) {
          queryParam += '&';
      }

      queryParam += 'state=' + data.state;
    }

    this.dataLoadFailed.next(false);
    this.authService.getAuthenticatedUser().getSession((err, session) => {
      this.http.get(this.fetchWard + queryParam, {
        headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
      })
        .subscribe(
          (data : WardData[]) => {
            if (!data) {
              this.dataLoadFailed.next(true);
              return;
            }
            this.wardData = data;
            this.dataLoaded.next(data);
          },
          (error) => {
            console.log(error);
            this.dataLoadFailed.next(true);
            this.dataLoaded.next(null);
          }
        );
    });
  }
  onDeleteData(data: WardData) {
    this.dataLoadFailed.next(false);
    this.authService.getAuthenticatedUser().getSession((err, session) => {
      this.http.post(this.deleteWard, data, {
        headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
      })
        .subscribe(
          (data) => {
            console.log(data);
          },
          (error) => this.dataLoadFailed.next(true)
        );
    });
  }

  onUpdateData(data: String) {
    this.dataLoadFailed.next(false);
    this.authService.getAuthenticatedUser().getSession((err, session) => {
        this.http.post(this.updateWard, data, {
            headers: this.httpHeaders.append('Authorization', session.getIdToken().getJwtToken())
        })
            .subscribe(
                (data) => {
                    console.log(data);
                },
                (error) => this.dataLoadFailed.next(true)
            );
    });
  }
}
